import {Component, OnInit} from '@angular/core';
import {GameService} from '../../services/game.service';
import {Observable} from 'rxjs';
import {stringify} from 'querystring';

@Component({
  selector: 'app-end-game',
  templateUrl: './end-game.component.html',
  styleUrls: ['./end-game.component.css']
})
export class EndGameComponent implements OnInit {
  public answers;
  public inviteUrl;
  public isUser2: boolean;

  constructor(private gameService: GameService) {
  }

  ngOnInit(): void {
    this.loadResults();
  }

  private async loadResults(): Promise<void> {
    this.isUser2 = await this.gameService.isUser2().toPromise();
    if (!this.isUser2) {
      this.generateInviteURL();
    }
    this.retrievePositiveAnswers();
  }

  retrievePositiveAnswers(): void {
    this.gameService.endGame().subscribe((answers) => {
      this.answers = answers;
    });
  }

  generateInviteURL(): void {
    const gameId = this.gameService.getCurrentGameId();
    this.inviteUrl = 'http://localhost:4200/game/second/' + gameId;
  }
}
