import {Component, OnInit} from '@angular/core';
import {GameService} from '../../services/game.service';
import {Game} from '../../model/Game';
import {TopicService} from '../../services/topic.service';
import {Topic} from '../../model/Topic';
import {Router} from '@angular/router';

@Component({
  selector: 'app-start-page',
  templateUrl: './start-page.component.html',
  styleUrls: ['./start-page.component.css'],
})

export class StartPageComponent implements OnInit {
  game: Game;
  topics: Topic[];

  constructor(private gameService: GameService,
              private topicService: TopicService,
              private router: Router,
  ) {
  }

  ngOnInit(): void {
    this.topicService.findAll().subscribe(
      (data) => {
        this.topics = data;
      }
    );
  }

  async startGame(topicId: string): Promise<void> {
    await this.gameService.startGame(topicId);
    this.router.navigate(['/game']);
  }
}
