import {Component, OnInit} from '@angular/core';
import {Answer} from '../../model/Answer';
import {GameService} from '../../services/game.service';
import {Router} from '@angular/router';

@Component({
  selector: 'app-answer-view',
  templateUrl: './answer-view.component.html',
  styleUrls: ['./answer-view.component.css']
})
export class AnswerViewComponent implements OnInit {
  answer: Answer;

  constructor(private gameService: GameService,
              private router: Router) {
  }

  ngOnInit(): void {
    this.getNextValue();
  }

  getNextValue(): void {
    this.gameService
      .getNextValue()
      .subscribe((answer) => {
        if (answer != null) {
          this.answer = answer;
        } else {
          this.finishGame();
        }
      });
  }

  async submitAnswer(answerId, yesOrNo): Promise<void> {
    await this.gameService.submitAnswer(answerId, yesOrNo).toPromise();
    this.getNextValue();
  }

  finishGame(): void {
    this.router.navigate(['/end']);
  }
}
