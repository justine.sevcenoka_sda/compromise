import {Component, OnInit} from '@angular/core';
import {GameService} from '../../services/game.service';
import {Answer} from '../../model/Answer';
import {ActivatedRoute, Router} from '@angular/router';

@Component({
  selector: 'app-second-player-start-game',
  templateUrl: './second-player-start-game.component.html',
  styleUrls: ['./second-player-start-game.component.css']
})
export class SecondPlayerStartGameComponent implements OnInit {
  public msg: string;
  answer: Answer;
  gameId: string;

  constructor(private gameService: GameService,
              private route: ActivatedRoute,
              private router: Router) {
  }

  ngOnInit(): void {
    const gameId = this.route.snapshot.params.id;
    this.gameService.startSecondGame(gameId).subscribe((msg) => {this.msg = msg; });
    console.log(this.msg);
  }

  startGame(): void {
    this.router.navigate(['/game']);
    this.gameService.getNextValue();
  }

}
