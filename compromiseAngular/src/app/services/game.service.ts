//todo uztaisīt, lai endGame prieks otrā user neģenerē invite linku
import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {Answer} from '../model/Answer';
import {Game} from '../model/Game';
import {Router} from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class GameService {
  private game: Game;
  private newUrl: string;
  private nextUrl: string;
  private registerUrl: string;
  private inviteUrl: string;
  private endUrl: string;
  private gameId: string;
  private secondUrl: string;
  private hasUser2Url: string;

  constructor(private http: HttpClient,
              private router: Router) {
    this.newUrl = 'http://localhost:8080/game/new/';
    this.nextUrl = 'http://localhost:8080/game/next/';
    this.registerUrl = 'http://localhost:8080/game/register/';
    this.inviteUrl = 'http://localhost:8080/game/invite/';
    this.endUrl = 'http://localhost:8080/game/end/';
    this.secondUrl = 'http://localhost:8080/game/second/';
    this.hasUser2Url = 'http://localhost:8080/game/hasuser2/';
  }

  public startGame(topicId: string): Promise<string> {
    this.game = new Game(topicId);
    return new Promise(
      resolve => this.http.get<string>(this.newUrl + topicId)
        .subscribe(
          (gameId) => {
            this.game.id = gameId;
            sessionStorage.setItem('currentGameId', gameId);
            resolve();
          }
        ));
  }

  public getNextValue(): Observable<Answer> {
    this.gameId = this.getCurrentGameId();
    return this.http.get<Answer>(this.nextUrl + this.gameId);
  }

  // tslint:disable-next-line:ban-types
  public submitAnswer(answerId: string, yesOrNo: string): Observable<Object> {
    this.gameId = this.getCurrentGameId();

    return this.http.get(this.registerUrl + this.gameId + '/' + answerId + '/' + yesOrNo);
  }

  public endGame(): Observable<Answer[]> {
    this.gameId = this.getCurrentGameId();
    return this.http.get<Answer[]>(this.endUrl + this.gameId);
  }

  public invitePartner(): Observable<string> {
    this.gameId = this.getCurrentGameId();
    return this.http.get<string>(this.inviteUrl + this.gameId);
  }

  startSecondGame(gameId: string): Observable<string> {
    sessionStorage.setItem('currentGameId', gameId);
    return this.http.get(this.secondUrl + gameId, {responseType: 'text'});
  }

  public isUser2(): Observable<boolean> {
    this.gameId = this.getCurrentGameId();
    return this.http.get<boolean>(this.hasUser2Url + this.gameId);
  }

  public getCurrentGameId(): string {
    return sessionStorage.getItem('currentGameId');
  }


}
