import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {Topic} from '../model/Topic';

@Injectable({
  providedIn: 'root'
})
export class TopicService {
  private url: string;

  constructor(private http: HttpClient) {
    this.url = 'http://localhost:8080/topics';
  }

  public findAll(): Observable<Topic []> {
    return this.http.get<Topic[]>(this.url);
  }
}
