import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class AuthenticationService {

  private url: string;

  constructor(private httpClient: HttpClient) {
    this.url = 'http://localhost:8080/user';
  }

  // tslint:disable-next-line:typedef
  authenticate(name, password) {
    const headers = new HttpHeaders({
      Authorization: 'Basic ' + btoa(name + ':' + password),
    });

    const
      promise = new Promise((resolve, reject) => {
        this.httpClient
          .get<string>('http://localhost:8080/user', {headers})
          .subscribe(
            (currentUser) => {
              console.log(currentUser);
              sessionStorage.setItem('username', currentUser);
              const authString = 'Basic ' + btoa(name + ':' + password);
              sessionStorage.setItem('basicauth', authString);
              resolve();
            }
          );
      });
    return promise;
  }


  isUserLoggedIn() {
    const user = sessionStorage.getItem('username');
    console.log(!(user === null));
    return !(user === null);
  }
}
