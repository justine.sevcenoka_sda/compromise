import {BrowserModule} from '@angular/platform-browser';
import {CommonModule} from '@angular/common';
import {NgModule} from '@angular/core';

import {AppRoutingModule} from './app-routing.module';
import {AppComponent} from './app.component';
import {StartPageComponent} from './components/start-page/start-page.component';
import {AnswerViewComponent} from './components/answer-view/answer-view.component';
import {HTTP_INTERCEPTORS, HttpClientModule} from '@angular/common/http';
import {LoginComponent} from './components/login/login.component';
import {FormsModule} from '@angular/forms';
import {UserService} from './services/user.service';
import {BasicAuthHttpInterceptorService} from './services/basic-auth-http-interceptor.service';
import {EndGameComponent} from './components/end-game/end-game.component';
import { SecondPlayerStartGameComponent } from './components/second-player-start-game/second-player-start-game.component';

@NgModule({
  declarations: [
    AppComponent,
    StartPageComponent,
    AnswerViewComponent,
    LoginComponent,
    EndGameComponent,
    SecondPlayerStartGameComponent
  ],
  imports: [
    CommonModule,
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    FormsModule,
  ],
  providers: [
    UserService,
    {
      provide: HTTP_INTERCEPTORS,
      useClass: BasicAuthHttpInterceptorService,
      multi: true
    }
  ],
  bootstrap: [AppComponent]
})
export class AppModule {
}
