import {User} from './User';
import {Observable, Subscription} from 'rxjs';

export class Game {
  id: string;
  topicId: string;
  user1: User;
  user2: User;
  answers1: [string];
  answers2: [string];

  constructor(topicId: string) {
    this.topicId = topicId;
  }
}
