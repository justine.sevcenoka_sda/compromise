export class Answer {
  answerId: string;
  name: string;
  url: string;
  imageUrl: string;
}

