import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {StartPageComponent} from './components/start-page/start-page.component';
import {AnswerViewComponent} from './components/answer-view/answer-view.component';
import {AuthGuardService} from './services/auth-guard.service';
import {LoginComponent} from './components/login/login.component';
import {EndGameComponent} from './components/end-game/end-game.component';
import {SecondPlayerStartGameComponent} from './components/second-player-start-game/second-player-start-game.component';

const routes: Routes = [

  {
    path: 'start',
    component: StartPageComponent,
    canActivate: [AuthGuardService]
  },
  {
    path: 'game',
    component: AnswerViewComponent,
    // canActivate: [AuthGuardService]
  },
  {
    path: 'login',
    component: LoginComponent
  },
  {
    path: 'end',
    component: EndGameComponent
  },
  {
    path: 'game/second/:id',
    component: SecondPlayerStartGameComponent
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {
}
