package sev.justine.compromise.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import sev.justine.compromise.model.Topic;
import sev.justine.compromise.repository.TopicRepository;

import java.util.List;

@Service
public class TopicService {
    private final TopicRepository topicRepository;

    @Autowired
    public TopicService(TopicRepository topicRepository) {
        this.topicRepository = topicRepository;
    }

    public Topic getTopic(Long topicId) {
        return topicRepository.findById(topicId).get();
    }

    public List<Topic> findAll() {
        return topicRepository.findAll();
    }
}
