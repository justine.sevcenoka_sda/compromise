package sev.justine.compromise.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import sev.justine.compromise.model.*;
import sev.justine.compromise.repository.GameRepository;

import java.util.List;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.stream.Collectors;

@Service
public class GameService {
    private final GameRepository gameRepository;
    private final TopicService topicService;
    private final UserService userService;
    private final AnswerService answerService;

    @Autowired
    public GameService(GameRepository gameRepository, TopicService topicService, UserService userService, AnswerService answerService) {
        this.gameRepository = gameRepository;
        this.topicService = topicService;
        this.userService = userService;
        this.answerService = answerService;
    }

    public Long createNewGame(Long topicId) {
        Topic topic = topicService.getTopic(topicId);
        User user = userService.getCurrentUser();
        Game game = new Game(topic, user);
        game = gameRepository.save(game);
        return game.getGameId();
    }

    public Answer getNextAnswer(Long gameId) {
        Game game = gameRepository.findById(gameId).get();

        List<Answer> registeredAnswerList;
        List<Answer> answerPool = game.getTopic().getAnswers();

        registeredAnswerList = userService.getCurrentUserAnswers(game)
                .stream()
                .map(toAnswer())
                .collect(Collectors.toList());

        return answerPool.stream()
                .filter(isNotIn(registeredAnswerList))
                .findFirst()
                .orElse(null);
    }

    public void submitAnswer(Long gameId, Long answerId, String trueFalse) {
        Answer answer = answerService.findById(answerId);
        Boolean bool = Boolean.parseBoolean(trueFalse);
        RegisteredAnswer newRegisteredAnswer = new RegisteredAnswer(answer, bool);
        Game game = gameRepository.findById(gameId).get();
        List<RegisteredAnswer> registeredAnswerList = userService.getCurrentUserAnswers(game);
        registeredAnswerList.add(newRegisteredAnswer);
        gameRepository.save(game);
    }

    public String generateInviteUrl(Long gameId) {
        return "http://localhost:8080/game/second/" + gameId;
    }

    public String startSecondGame(Long gameId) {
        Game game = gameRepository.findById(gameId).get();
        User user2 = new User();
        userService.save(user2);
        game.setUser2(user2);
        gameRepository.save(game);
        return game.getUser1().getEmail() + " invited you to agree on " + game.getTopic().getName();
    }

    public List<Answer> returnMatchedAnswers(Long gameId) {
        Game game = gameRepository.findById(gameId).get();
        List<Answer> trueAnswerListUser1 = getTrueAnswerList(game.getUser1(), gameId);
        List<Answer> trueAnswerList;

        if (isSinglePlayer(game)) {
            trueAnswerList = trueAnswerListUser1;
        } else {
            List<Answer> trueAnswerListUser2 = getTrueAnswerList(game.getUser2(), gameId);
            trueAnswerList = trueAnswerListUser1.stream()
                    .filter(answer -> trueAnswerListUser2.contains(answer))
                    .collect(Collectors.toList());
        }
        return trueAnswerList;
    }

    public Boolean hasUser2(Long gameId) {
        return gameRepository.findById(gameId).get().getUser2() != null;
    }

    private Function<RegisteredAnswer, Answer> toAnswer() {
        return registeredAnswer -> registeredAnswer.getAnswer();
    }

    private Predicate<Answer> isNotIn(List<Answer> registeredAnswerList) {
        return answer -> !registeredAnswerList.contains(answer);
    }

    private boolean isSinglePlayer(Game game) {
        return game.getUser2() == null;
    }

    private List<Answer> getTrueAnswerList(User user, Long gameId) {
        Game game = gameRepository.findById(gameId).get();
        return userService.getUserAnswers(user, game)
                .stream()
                .filter(hasSelected())
                .map(toAnswer())
                .collect(Collectors.toList());
    }
    private Predicate<RegisteredAnswer> hasSelected() {
        return registeredAnswer -> registeredAnswer.isSelected();
    }
}
