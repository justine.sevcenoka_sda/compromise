package sev.justine.compromise.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import sev.justine.compromise.model.Answer;
import sev.justine.compromise.repository.AnswerRepository;

import java.io.*;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

@Service
public class CSVReader {
    private BufferedReader br = null;
    private String line = "";
    private String csvSplitBy = ",";
    AnswerRepository answerRepository;

    @Autowired
    public CSVReader(AnswerRepository answerRepository) {
        this.answerRepository = answerRepository;
    }

    private File getFileFromResource(String fileName) throws URISyntaxException {
        ClassLoader classLoader = getClass().getClassLoader();
        URL resource = classLoader.getResource(fileName);
        if (resource == null) {
            throw new IllegalArgumentException("file not found ! " + fileName);
        } else {
            return new File(resource.toURI());
        }
    }

    public List<Answer> readAnswersFromFile(String fileName) {
        List<Answer> result = new ArrayList();
        try (BufferedReader br = new BufferedReader(new FileReader(getFileFromResource("static/" + fileName)))) {
            while ((line = br.readLine()) != null) {
                String[] answerString = line.split(csvSplitBy);
                Answer answer = new Answer(answerString[0], answerString[1], answerString[2]);
                result.add(answer);
            }
        } catch (IOException | URISyntaxException e) {
            e.printStackTrace();
        } finally {
            if (br != null) {
                try {
                    br.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
        return result;
    }
}
