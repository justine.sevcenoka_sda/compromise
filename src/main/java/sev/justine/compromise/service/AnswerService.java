package sev.justine.compromise.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import sev.justine.compromise.model.Answer;
import sev.justine.compromise.repository.AnswerRepository;

import java.util.List;

@Service
public class AnswerService {
    private final AnswerRepository answerRepository;

    @Autowired
    public AnswerService(AnswerRepository answerRepository) {
        this.answerRepository = answerRepository;
    }

    public List<Answer> findAll() {
        return answerRepository.findAll();
    }

    public Answer findById(Long answerId) {
        return answerRepository.findById(answerId).get();
    }
}
