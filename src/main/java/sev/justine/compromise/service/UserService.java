package sev.justine.compromise.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;
import sev.justine.compromise.model.Game;
import sev.justine.compromise.model.RegisteredAnswer;
import sev.justine.compromise.model.User;
import sev.justine.compromise.repository.UserRepository;

import java.util.List;

@Service
public class UserService {

    private final UserRepository userRepository;

    @Autowired
    public UserService(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    public User getCurrentUser() {
        return userRepository.findByEmail(SecurityContextHolder
                .getContext()
                .getAuthentication()
                .getName());
    }

    public List<RegisteredAnswer> getCurrentUserAnswers(Game game) {
        return game.getUser1() == getCurrentUser() ? game.getAnswers1() : game.getAnswers2();
    }

    public List<RegisteredAnswer> getUserAnswers(User user, Game game) {
        return game.getUser1() == user ? game.getAnswers1() : game.getAnswers2();
    }

    public void save(User user) {
        userRepository.save(user);
    }
}
