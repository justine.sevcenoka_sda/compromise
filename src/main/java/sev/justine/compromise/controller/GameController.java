package sev.justine.compromise.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import sev.justine.compromise.model.Answer;
import sev.justine.compromise.service.GameService;

import java.util.List;

@CrossOrigin
@RestController
public class GameController {
    private GameService gameService;

    @Autowired
    public GameController(GameService gameService) {
        this.gameService = gameService;
    }

    @GetMapping("/game/new/{topicId}")
    public Long startGame(@PathVariable("topicId") Long topicId) {
        return gameService.createNewGame(topicId);
    }

    @GetMapping("/game/next/{gameId}")
    public Answer getNextValue(@PathVariable("gameId") Long gameId) {
            return gameService.getNextAnswer(gameId);
    }

    @GetMapping("/game/register/{gameId}/{answerId}/{yesOrNo}")
    public void submitAnswer(@PathVariable("gameId") Long gameId, @PathVariable("answerId") Long answerId, @PathVariable("yesOrNo") String yesOrNo) {
        gameService.submitAnswer(gameId, answerId, yesOrNo);
    }

    @GetMapping("/game/invite/{gameId}")
    public String invitePartner(@PathVariable("gameId") Long gameId) {
        return gameService.generateInviteUrl(gameId);
    }

    @GetMapping("/game/end/{gameId}")
    public List<Answer> endGame(@PathVariable("gameId") Long gameId) {
        return gameService.returnMatchedAnswers(gameId);
    }

    @GetMapping("/game/second/{gameId}")
    public Object startSecondGame(@PathVariable("gameId") Long gameId) {
        return gameService.startSecondGame(gameId);
    }

    @GetMapping("/game/hasuser2/{gameId}")
    public Boolean hasUser2(@PathVariable("gameId") Long gameId) {
       return gameService.hasUser2(gameId);
    }
}
