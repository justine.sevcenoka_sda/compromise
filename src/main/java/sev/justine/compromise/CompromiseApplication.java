package sev.justine.compromise;

import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import sev.justine.compromise.model.Answer;
import sev.justine.compromise.model.Topic;
import sev.justine.compromise.model.User;
import sev.justine.compromise.repository.AnswerRepository;
import sev.justine.compromise.repository.TopicRepository;
import sev.justine.compromise.repository.UserRepository;
import sev.justine.compromise.service.CSVReader;

import java.util.List;
import java.util.stream.Stream;

@SpringBootApplication
public class CompromiseApplication {

    public static void main(String[] args) {
        SpringApplication.run(CompromiseApplication.class, args);
    }

    @Bean
    CommandLineRunner init(UserRepository userRepository, TopicRepository topicRepository, AnswerRepository answerRepository, CSVReader csvReader) {
        return args -> {
            Stream.of("John@mail.com", "a@mail.com", "Julie@mail.com", "Jennifer@mail.com", "Helen@mail.com", "Rachel@mail.com").forEach(email -> {
                User user = new User(email.toLowerCase(), email.toLowerCase() + "Pass");
                user = userRepository.save(user);
            });

            Stream.of("Cities", "Movies", "Dinner").forEach(topicName -> {
                Topic topic = new Topic(topicName);
                List<Answer> answers = csvReader.readAnswersFromFile("AnswerPool" + topicName + ".csv");
                answers = answerRepository.saveAll(answers);
                topic.setAnswers(answers);
                topicRepository.save(topic);
            });
        };
    }
}
