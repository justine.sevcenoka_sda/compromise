package sev.justine.compromise.model;

import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.*;
import java.util.List;

@Entity
@Getter
@Setter
@RequiredArgsConstructor
@ToString
public class Game {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long gameId;

    @ManyToOne
    @JoinColumn(name = "topicId")
    private Topic topic;

    @ManyToOne
    @JoinColumn(name = "userId1", referencedColumnName = "userId")
    private User user1;

    @ManyToOne
    @JoinColumn(name = "userId2", referencedColumnName = "userId")
    private User user2;

    @OneToMany(cascade = CascadeType.ALL)
    List<RegisteredAnswer> answers1;
    @OneToMany(cascade = CascadeType.ALL)
    List<RegisteredAnswer> answers2;

    public Game(Topic topic, User user1) {
        this.topic = topic;
        this.user1 = user1;
    }
}
