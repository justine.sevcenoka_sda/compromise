package sev.justine.compromise.model;

import lombok.*;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
@Getter
@Setter
@RequiredArgsConstructor
@ToString
@Table(name = "TOPIC")
public class Topic {
    @Id
    @GeneratedValue
    @Column(name = "TOPIC_ID")
    private Long topicId;

    private String name;

    @ManyToMany(fetch = FetchType.EAGER)
    @JoinTable(
            name = "TOPIC_ANSWER",
            joinColumns = {@JoinColumn(name = "TOPIC_ID")},
            inverseJoinColumns = {@JoinColumn(name = "ANSWER_ID")}
    )
    private List<Answer> answers = new ArrayList<>();

    public Topic(String name) {
        this.name = name;
    }
}
