package sev.justine.compromise.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.*;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
@Getter
@Setter
@AllArgsConstructor
@RequiredArgsConstructor
@Table(name = "ANSWER")
public class Answer {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "ANSWER_ID")
    private Long answerId;

    private String name;
    private String url;
    private String imageUrl;

    @JsonIgnore
    @ManyToMany(fetch = FetchType.EAGER, mappedBy = "answers", cascade = CascadeType.ALL)
    private List<Topic> topics = new ArrayList<>();

    @OneToMany
    private List<RegisteredAnswer> registeredAnswers;

    public Answer(String name, String url, String imageUrl) {
        this.name = name;
        this.url = url;
        this.imageUrl = imageUrl;
    }

    @Override
    public String toString() {
        return "Answer{" +
                "answerId=" + answerId +
                ", name='" + name + '\'' +
                ", url='" + url + '\'' +
                ", imageUrl='" + imageUrl + '\'' +
                ", registeredAnswers=" + registeredAnswers +
                '}';
    }
}
