package sev.justine.compromise.model;

import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.*;

@Entity
@Getter
@Setter
@RequiredArgsConstructor
public class RegisteredAnswer {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long registeredAnswerId;

    @ManyToOne
    private Answer answer;

    private boolean selected;

    public RegisteredAnswer(Answer answer, boolean selected) {
        this.answer = answer;
        this.selected = selected;
    }
}
