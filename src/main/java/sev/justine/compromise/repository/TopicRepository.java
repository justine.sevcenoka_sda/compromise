package sev.justine.compromise.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import sev.justine.compromise.model.Topic;

@Repository
public interface TopicRepository extends JpaRepository<Topic, Long> {
}
