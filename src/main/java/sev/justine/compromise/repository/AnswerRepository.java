package sev.justine.compromise.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import sev.justine.compromise.model.Answer;

@Repository
public interface AnswerRepository extends JpaRepository<Answer, Long> {
}
