package sev.justine.compromise.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import sev.justine.compromise.model.RegisteredAnswer;

@Repository
public interface RegisteredAnswerRepository extends JpaRepository<RegisteredAnswer, Long> {
}
