package sev.justine.compromise.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import sev.justine.compromise.model.Game;

@Repository
public interface GameRepository extends JpaRepository<Game, Long> {
}
